import struct
import os
import random
import uuid
import sys

MAGIC_VALUE_IGNORE = 0x55555555

def is_py3():
    return sys.version_info >= (3, 0)

def getInt(data):
    return struct.unpack("<I", data[:4])[0]

def getIntArray(data):
    array = []
    for i in range(0, len(data), 4):
        newInt = struct.unpack("<I", data[i:i+4])[0]
        array.append(newInt)

    return array

def mutate_file(filepath):
    print("mutating {}".format(filepath))
    randomGen = random.SystemRandom()
    with open(filepath, "rb") as f:
        filebytes = bytearray(f.read())

        num_change = int(len(filebytes)*(0.05))
        for i in range(num_change):
            index = randomGen.randint(0, len(filebytes)-1)
            filebytes[index] = randomGen.randint(0, 255)

    filename = generate_random_filename(filepath)

    with open(filename, "wb") as f:
        f.write(filebytes)

    return filename

def generate_random_filename(filepath):
    return "".join(filepath.split("_")[:-1])+"_"+uuid.uuid4().hex

def create_random_file(output_dir, size):
    randomGen = random.SystemRandom()

    random_file = os.path.join(output_dir, str(uuid.uuid4().hex))

    with open(random_file, "wb") as f:
        for i in range(0, size, 4):
            f.write(struct.pack("<I", randomGen.randint(0, 0xffffffff)))

    return random_file

def generate_from_magic_cmps(magic_cmps, input_file):
    with open(input_file, "rb") as f:
        input_bytes = bytearray(f.read())
        tmp = bytearray(input_bytes)

        for j in range(1):
            for i, values in magic_cmps.iteritems():
                #values.add(tmp[i])
                #value = random.sample(values, 1)[0]
                value = values[-1]

                if value != MAGIC_VALUE_IGNORE:
                    tmp[i] = value

            new_filename = os.path.join(os.path.dirname(input_file),
                    generate_random_filename(input_file))
            print("generated from magic cmps: {}".format(new_filename))
            with open(new_filename, "wb") as newf:
                newf.write(tmp)

def explicit_explore(project, state, sm, find_addr, hitcount):

    while sm.active:
        print(sm)
        print(sm.active)

        for s in sm.active:
            print("explicit step @ {}".format(s.ip))
            print_state(s)

            try:
                print(project.factory.block(state.se.eval(s.ip)).capstone.pp())
            except Exception as e:
                print(e)

            print("end explicit step")
            print("=================")

            if state.se.eval(s.ip) == find_addr:
                return s

        for err in sm.errored:
            print(err)


        sm.step()

    print("sm explore returned")
    print(sm)


def print_state(state):
    print(state)
    regs = {
            'eax': state.regs.eax,
            'ebx': state.regs.ebx,
            'ecx': state.regs.ecx,
            'edx': state.regs.edx,
            'edi': state.regs.edi,
            'esi': state.regs.esi,
            'esp': state.regs.esp,
    }

    for reg, value in regs.iteritems():
        print("{}: {}".format(reg, value))
