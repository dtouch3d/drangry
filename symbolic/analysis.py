#!/usr/bin/env python2

from __future__ import print_function

import sys
import os
import uuid
import binascii

try:
    import queue
except ImportError:
    import Queue as queue


def marksman(project, state, workq, output, avoid):
    print("project {}, state: {}".format(project, state))

    while True:

        try:
            cbr = workq.get(timeout=21)
        except queue.Empty:
            print("queue is empty and hit timeout, worker exiting ...")
            return

        state.input_info.hitcount = cbr.hitcount

        sm = project.factory.simulation_manager(state)

        print("finding address {}".format(hex(cbr.search_address)))
        #angr.loggers.setall(logging.DEBUG)
        sm.explore(find=cbr.search_address, avoid=avoid)
        #explicit_explore(project, state, sm, search_address, hitcount)

        print("sm ended for addr {} : {}".format(hex(cbr.search_address), sm))

        if sm.errored:
            for error_state in sm.errored:
                print(error_state)

        try:
            for found in sm.found:
                solve_state(project, found, output)
        except Exception:
            print("pre solve_state exception")
            print(sys.exc_info())


def solve_state(project, state, output):
    print("~ state @ {} solution ~".format(state.regs.ip))
    print(state.input_info)

    input_buf = state.input_info.address
    input_size = state.input_info.size
    file_offset = state.input_info.offset
    input_bytes = state.input_info.bytes

    bv_input = state.memory.load(input_buf, input_size)
    solution = state.solver.eval(bv_input, cast_to=bytes)

    print("Got solution for {}: {}, file offset {}".format(state.regs.ip, binascii.hexlify(solution), file_offset))

    filename = "{}_{}".format(os.path.join(output,
        os.path.basename(project.filename)), uuid.uuid4().hex)

    with open(filename, 'wb') as outfile:
        if file_offset > 0:
            print("writing from concrete input until {}".format(file_offset))
            outfile.write(input_bytes[:file_offset])

        outfile.write(solution)
        outfile.write(input_bytes[len(solution)+file_offset:])
