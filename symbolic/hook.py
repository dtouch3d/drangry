#!/usr/bin/env python2

from __future__ import print_function

import angr
import archinfo
from .utils import is_py3

class InputFunc(angr.SimProcedure):
    MAX_INPUT_SIZE = 1024

    def prepare_input(self, buf, total_size):

        total_size = min(total_size, self.MAX_INPUT_SIZE)

        self.state.input_info.address = self.state.solver.eval(buf)
        self.state.input_info.size = total_size

        symbolize(self.state, buf, total_size)

        ret = self.state.solver.BVV(total_size, 8*4)

        return ret


class InputInfoStatePlugin(angr.SimStatePlugin):
    def __init__(self, input_address, input_bytes, filename,
            size=InputFunc.MAX_INPUT_SIZE, offset=0, hitcount=None):

        super(InputInfoStatePlugin, self).__init__()
        self.address = input_address
        self.bytes = input_bytes

        self.size = size

        self.offset = offset
        self.filename = filename
        self.hitcount = hitcount

        self.copied = 0

    @angr.SimStatePlugin.memo
    def copy(self, memo):
        obj = InputInfoStatePlugin(self.address, self.bytes, self.filename,
                self.size, self.offset, self.hitcount)

        self.copied = 1

        return obj

    def __str__(self):
        return "< InputInfoStatePlugin: address: {}, size: {} copied: {} >" \
               .format(hex(self.address), self.size, self.copied)

class RetNum(angr.SimProcedure):

    def __init__(self, num):
        super(RetNum, self).__init__()
        self.num = num

    def run(self):
        return self.num

class ReadHook(InputFunc):
    def run(self, fd, sym_buf, sym_size):
        print("read({}, {}, {})".format(fd, sym_buf, sym_size))

        buf = self.state.solver.eval(sym_buf)
        total_size = self.state.solver.eval(sym_size)

        return self.prepare_input(buf, total_size)


class FreadHook(InputFunc):
    def run(self, sym_buf, sym_size, sym_nmemb, file_ptr):
        print("fread({}, {}, {}, {}) ret: {}".format(sym_buf, sym_size, sym_nmemb,
            file_ptr, sym_size*sym_nmemb))

        buf = self.state.solver.eval(sym_buf)
        total_size = self.state.solver.eval(sym_nmemb*sym_size)

        return self.prepare_input(buf, total_size)

class CallNativeHook(angr.SimProcedure):
    IS_FUNCTION = True

    def __init__(self, addr):
        super(CallNativeHook, self).__init__()
        self.addr = addr
        self.retaddr = None

    def run(self):
        self.retaddr = self.state.memory.load(self.state.regs.esp, 4,
                endness=archinfo.Endness.LE)

        print("CallNativeHook calling: {}, retaddr: {}".format(hex(self.addr),
            self.retaddr))

        self.jump(self.addr)

def symbolize(state, addr, size):
    hitcount = state.input_info.hitcount
    input_bytes = state.input_info.bytes
    offset = state.input_info.offset

    for i in range(size):
        if i < len(hitcount) and hitcount[i] != 0:
            sym_byte = state.solver.BVS("input_byte{}".format(i), 8)
            state.memory.store(addr+i, sym_byte)
        else:
            state.memory.store(addr+offset+i, input_bytes[offset+i])

def get_exit_func(obj):
    plt = obj.plt

    if 'exit' in plt:
        return plt['exit']

    return None

def imports_fixup(project, bin_object):
    print("imports fixup obj {}".format(bin_object))

    plt = bin_object.plt
    print("plt: {}".format(plt))
    hook_blacklist_plt(project, plt)

    for func in plt:
        try:
            if not func.startswith("_") and not project.is_hooked(plt[func]):
                symbol = project.loader.find_symbol(func)

                if is_py3():
                    owner = symbol.owner.binary
                else:
                    owner = symbol.owner_obj.binary

                if "libc-" not in owner:
                    print("hooking plt func {} with symbol {} @ {}".format(func, symbol, symbol.rebased_addr))
                    project.hook(plt[func], CallNativeHook(symbol.rebased_addr))

        except Exception as e:
            print("custom lib hook exception! {}".format(e))

    for func in plt:
        try:
            if not func.startswith("_") and not project.is_hooked(plt[func]):
                symbol = project.loader.find_symbol(func)

                if is_py3():
                    owner = symbol.owner.binary
                else:
                    owner = symbol.owner_obj.binary


                if "libc-" in owner:
                    print("hooking LIBC plt func {} with symbol {} @ {}".format(func, symbol, symbol.rebased_addr))
                    project.hook(plt[func], angr.SIM_PROCEDURES['libc'][func]())
        except Exception as e:
            print("libc hook exception! {}".format(e))
            pass

def hook_blacklist_plt(project, plt):

    blacklist = {
        "read": ReadHook(),
        "fread": FreadHook(),
        "fprintf": RetNum(16),
        "printf": RetNum(16),
    }

    for func in blacklist.keys():
        if func in plt.keys():
            print("blacklist hooking plt func {} @ {}".format(func, plt[func]))
            project.hook(plt[func], blacklist[func])

    return
