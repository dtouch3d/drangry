import angr
from networkx.algorithms.traversal.depth_first_search import dfs_tree

def get_subtree(graph, block):
    print("getting subtree ...")
    return dfs_tree(graph, block)


def get_addr_block(cfg, addr):
    print("pre get_by_addr for pc {}".format(hex(addr)))

    func = None

    """
    We search for the function that corresponds to that particular address. Rather
    tragically the CFG only holds information for every start address of function
    and it appears we have to search by hand. We search quickly by querying for a
    function start up to 128 bytes before address, this is fairly fast because it
    performs a dictionary lookup.
    """

    for i in range(128):
        try:
            func = cfg.functions.get_by_addr(addr-i)
            break
        except KeyError:
            pass


    if func is not None:
        print("found func!: {}".format(func))
        for block in func.nodes:
            if block.addr == addr:
                print("Found node: {}".format(block))
                return func, block

    return None, None


def get_address_priority(cfg, addr):
    func, block = get_addr_block(cfg, addr)

    print("[cfg] got func {}, block {}".format(func, block))
    if block:
        return len(get_subtree(func.graph, block))

    return 0
