#!/usr/bin/env python2

from __future__ import print_function

import struct
import multiprocessing
import os
import sys
import shutil
import time
import traceback
from collections import OrderedDict, defaultdict

from twisted.internet.protocol import Factory
from twisted.protocols.basic import NetstringReceiver
from twisted.internet.endpoints import UNIXServerEndpoint
from twisted.internet import reactor
from twisted.python import log

import angr
import symbolic.analysis
from symbolic.utils import getInt, getIntArray, is_py3
from symbolic.hook import imports_fixup, get_exit_func, InputInfoStatePlugin

UNIX_SOCKET_PATH = "/tmp/dreyfuzz.sock"

class CBRInfo(object):
    """ Keeps info about branch instruction

    From DynamoRIO we take the branch target, fallback,
    if the branch was taken and the address of the instruction.

    """

    def __init__(self, data, hitcount):

        members = getIntArray(data)

        assert len(members) == 4

        self.instr_address = members[0]
        self.target = members[1]
        self.fallback = members[2]
        self.taken = members[3]

        if self.taken:
            self.search_address = self.fallback
        else:
            self.search_address = self.target

        if hitcount:
            self.hitcount = getIntArray(hitcount)


class ProjectWrapper(object):
    """ Wrapper for project and state objects used for analysis """

    def __init__(self):
        self.project = None
        self.state = None
        self.avoid = []
        self.main_module_start = 0
        self.first_run = 1
        self.explored = OrderedDict()
        self.libopts = {}

        self.file_offset = 0
        self.binary = None
        self.input_file = None

        self.pool = []
        self.queue = multiprocessing.Queue()

        self.num_procs = 2
        self.started_workers = 0


    def project_init(self, binary, main_module_start, num_procs):
        print("Got module start: {}".format(hex(main_module_start)))
        import platform
        print("version: {}".format(platform.python_version()))


        print("libopts: {}".format(self.libopts))

        try:
            self.libopts.pop(os.path.basename(binary))
        except:
            pass

        # XXX: Attribute 'custom_base_addr' is just
        # 'base_addr' in py3

        base_opt = "custom_base_addr"

        if is_py3():
            base_opt = "base_addr"

        load_options = {
            'main_opts': {
                base_opt : main_module_start
            },
            'lib_opts': self.libopts
        }

        print("load_options: {}".format(load_options))

        print("self.binary: {}".format(binary))
        self.binary = binary
        self.project = angr.Project(binary, load_options=load_options)
        self.num_procs = num_procs

        #print("starting CFGFast analysis")
        #self.cfg = Maniae.pjw.project.analyses.CFGFast()
        #print("CFGFast analysis ended")

        shared_objs = self.project.loader.shared_objects
        print("shared objs: {}".format(shared_objs))
        shared_objs = [shared_objs[name] for name in shared_objs]

        for obj in shared_objs:
            exit_addr = get_exit_func(obj)

            if exit_addr is not None:
                self.avoid.append(exit_addr)

            print("avoid exit funcs: {}".format([hex(i) for i in self.avoid]))
            imports_fixup(self.project, obj)


    def add_library(self, name, start_va):

        base_opt = "custom_base_addr"

        # FIXME: Library name has an extra "'" character in it so
        # we ignore that for now. Also in py3 the load address attribute
        # is 'base_addr'
        if is_py3():
            name = name[:-1]
            base_opt = "base_addr"

        self.libopts[name] = {base_opt: start_va}
        print("lib name: {}, start: {}".format(name, hex(start_va)))


    def set_registers(self, regs):
        self.state.regs.eax = regs[0]
        self.state.regs.ebx = regs[1]
        self.state.regs.ecx = regs[2]
        self.state.regs.edx = regs[3]
        self.state.regs.esi = regs[4]
        self.state.regs.edi = regs[5]
        self.state.regs.ebp = regs[6]
        self.state.regs.esp = regs[7]+4


    def add_cbr(self, cbr):
        addr = cbr.search_address

        if addr-self.main_module_start not in self.explored:
            self.explored[addr-self.main_module_start] = cbr
            self.queue.put(cbr)


    def store_memory(self, addr, data):
        offset = 0

        gotplt = self.project.loader.main_object.sections_map.get('.got.plt', None)

        if gotplt and addr <= gotplt.vaddr < addr+len(data):
            offset = gotplt.memsize
            print("skipping {} bytes for plt @ {}".format(offset, gotplt.vaddr))

        self.state.memory.store(addr+offset, data[offset:])


    def start_process(self, input_buffer):

        if self.started_workers >= self.num_procs:
            return

        print("input buf @ {}".format(hex(input_buffer)))

        with open(self.input_file, "rb") as input_file:
            input_bytes = input_file.read()

        info_plugin = InputInfoStatePlugin(input_buffer, input_bytes,
            self.input_file, offset=self.file_offset)

        self.state.register_plugin('input_info', info_plugin)

        self.started_workers += self.num_procs

        for i in range(self.num_procs):
            process = multiprocessing.Process(target=symbolic.analysis.marksman,
                        args=(self.project, self.state, self.queue, self.output,
                        self.avoid))

            self.pool.append(process)
            process.start()


class Maniae(NetstringReceiver):
    """ Twisted server for handling incoming data from DynamoRIO.

    This class is responsible for communication with DynamoRIO.
    We have set up a primitive protocol with commands and for each
    one we setup a handler.

    """

    INITIALIZE_CMD                  = 0
    SEND_STATE_MEMORY_CMD_HEADER    = 1
    SEND_STATE_REGS_CMD             = 2
    SEND_CBR_CMD                    = 3
    SEND_TAINT_HIT_COUNT_CMD        = 4
    CONTINUE_CMD                    = 5
    SEND_STATE_MEMORY_CMD           = 6
    INPUT_BUFFER_CMD                = 7
    SEND_READ_RETADDR_CMD           = 8
    INPUT_FILE_CMD                  = 9
    FILE_OFFSET_CMD                 = 10
    SEND_LIB_INFO_CMD               = 11
    SEND_MAGIC_INPUT_CMPS           = 12

    # Static object that holds our project. This is the same
    # for every Twisted server object and we assume that we analyse
    # one binary at a time.

    pjw = ProjectWrapper()

    def __init__(self, factory):
        print("protocol init")

        # Increase Twisted's internal message size limit
        self.MAX_LENGTH = 1024*1024

        self.binary = factory.binary
        self.output = factory.output
        self.num_procs = factory.num_procs
        self.snapshot = factory.snapshot
        self.taint = factory.taint

        Maniae.pjw.output = self.output

        self.next_memory_segment_start = 0
        self.cbr_data = None

        self.libopts = {}

        self.magic_cmps = defaultdict(list)

        self.cmd2func = {
            Maniae.INITIALIZE_CMD : self.handle_init,
            Maniae.SEND_STATE_MEMORY_CMD_HEADER: self.handle_send_state_header,
            Maniae.SEND_STATE_REGS_CMD: self.handle_state_regs,
            Maniae.SEND_CBR_CMD: self.handle_cbr,
            Maniae.SEND_TAINT_HIT_COUNT_CMD: self.handle_hitcount,
            Maniae.SEND_STATE_MEMORY_CMD: self.handle_send_state,
            Maniae.INPUT_BUFFER_CMD: self.handle_input_buffer,
            Maniae.SEND_READ_RETADDR_CMD: self.handle_retaddr,
            Maniae.INPUT_FILE_CMD: self.handle_input_file,
            Maniae.FILE_OFFSET_CMD: self.handle_file_offset,
            Maniae.SEND_LIB_INFO_CMD: self.handle_lib_info,
            Maniae.SEND_MAGIC_INPUT_CMPS: self.handle_magic_cmps
        }

        #l = logging.getLogger('angr.manager').setLevel(logging.WARNING)
        #l = logging.getLogger('angr.engines.vex.engine').setLevel(logging.ERROR)

    def connectionMade(self):
        print("got new connection!")
        Maniae.pjw.first_run = Maniae.pjw.state is None
        print("first_run: {}".format(Maniae.pjw.first_run))
        self.transport.write(struct.pack("<I", Maniae.pjw.first_run))


    def connectionLost(self, reason):
        print("connection lost!")
        print("reason: {}".format(reason))

        print("magic cmp 0: {}".format(self.magic_cmps[0]))

        #if Maniae.pjw.first_run:
            #generate_from_magic_cmps(self.magic_cmps, self.input_file)


    def handle_init(self, data):
        main_module_start = getInt(data)
        Maniae.pjw.project_init(self.binary, main_module_start, self.num_procs)


    def handle_magic_cmps(self, data):
        magic_cmps = getIntArray(data)

        for i, value in enumerate(magic_cmps):
            self.magic_cmps[i].append(value)


    def handle_lib_info(self, data):
        start_va = getInt(data)
        name = os.path.basename(str(data[4:]))

        Maniae.pjw.add_library(name, start_va)


    def handle_retaddr(self, data):
        retaddr = getInt(data)-5

        if Maniae.pjw.state is not None:
            assert retaddr == Maniae.pjw.state.regs.pc

        else:
            Maniae.pjw.state = Maniae.pjw.project.factory.blank_state(
                addr=retaddr
                #, add_options=angr.options.unicorn
            )


    def handle_send_state_header(self, data):
        self.next_memory_segment_start = getInt(data)
        print("self.next_memory_segment_start: {}".format(hex(self.next_memory_segment_start)))


    def handle_send_state(self, data):
        if self.snapshot:
            print("storing memory {} ... ".format(self.snapshot))
            Maniae.pjw.store_memory(self.next_memory_segment_start, data)


    def handle_state_regs(self, data):
        regs = getIntArray(data)
        if self.snapshot:
            print("storing registers {} ... ".format(self.snapshot))
            Maniae.pjw.set_registers(regs)


    def handle_cbr(self, data):
        self.cbr_data = data


    def handle_hitcount(self, data):
        if self.taint:
            hitcount = data
        else:
            print("disabled taint ...")
            hitcount = [999]*len(getIntArray(data))

        cbr = CBRInfo(self.cbr_data, hitcount)
        Maniae.pjw.add_cbr(cbr)


    def handle_input_buffer(self, data):
        input_buffer = getInt(data)
        Maniae.pjw.start_process(input_buffer)


    def handle_input_file(self, data):
        input_file = data
        Maniae.pjw.input_file = input_file
        print("got input_file: {}".format(input_file))


    def handle_file_offset(self, data):
        file_offset = getInt(data)
        Maniae.pjw.file_offset = file_offset
        print("got file offset: {}".format(file_offset))


    def stringReceived(self, data):
        cmd = getInt(data)
        try:
            self.cmd2func[cmd](data[4:])
        except Exception:
            print("cm2func caught exception!")
            #print(sys.exc_info())
            traceback.print_exc(file=sys.stdout)

class ManiaeFactory(Factory):
    protocol = Maniae

    def __init__(self, binary, output, num_procs, snapshot, taint):
        self.binary = binary
        self.output = output
        self.num_procs = num_procs
        self.snapshot = snapshot
        self.taint = taint


    def buildProtocol(self, addr):
        return Maniae(self)


def start(binary, output, num_procs=1, snapshot=True, taint=True):

    if os.path.isfile("maniae.log"):
        shutil.copyfile("maniae.log",
                "maniae-workers-{}-snapshot-{}-taint-{}-{}.log".format(
                    num_procs, snapshot, taint, time.time()))

    log.startLogging(open("maniae.log", "w"))

    if os.path.exists(UNIX_SOCKET_PATH):
        os.unlink(UNIX_SOCKET_PATH)

    endpoint = UNIXServerEndpoint(reactor, UNIX_SOCKET_PATH)
    endpoint.listen(ManiaeFactory(binary, output, num_procs, snapshot, taint))
    reactor.run()
