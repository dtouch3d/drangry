#!/usr/bin/env python

"""
Analyze BINARY and generate output files at OUTPUT_DIR
that maximise the code coverage of the file.

Usage: ./drangry.py [options] <binary> <output_dir>

Arguments:
    binary         Path to binary program to analyze
    output_dir     Path to save generated input files

Options:
    --workers=<workers>     Number of parallel symbolic worker processes [default: 1]
    --snapshot=<True|False>    Enables snapshot from dynamic analyslis [default: True]
    --taint=<True|False>    Enables snapshot from dynamic analyslis [default: True]
    -h --help      Show this help message
"""


from __future__ import print_function

import os
import subprocess
import multiprocessing
import time

import pyinotify
from docopt import docopt

from symbolic import server
from symbolic import utils

BUILD_DIR = os.getcwd()+"/native/build/"
PREBUILTS_DIR = os.getcwd()+"/native/prebuilts"
DYNAMORIO_DIR = PREBUILTS_DIR+"/dynamorio"
DRMEMORY_DIR = PREBUILTS_DIR+"/drmemory"

MAX_FILE_SIZE = 1024*10

class NewTestcaseHandler(pyinotify.ProcessEvent):
    def __init__(self, binary):
        self.binary = binary
        super(NewTestcaseHandler, self).__init__()

    def process_IN_CREATE(self, event):
        print("testing {}".format(event.pathname))
        run_native(self.binary, event.pathname)

        time.sleep(4)
        #mutate_file(event.pathname)

# Run binary through DynamoRIO

def run_native(binary, input_file=None, output_dir=None):

    if not input_file:
        input_file = utils.create_random_file(output_dir, MAX_FILE_SIZE)

    if not output_dir:
        output_dir = os.path.dirname(input_file)


    args = ["setarch", "i386", "--addr-no-randomize",
            DYNAMORIO_DIR+"/bin32/drrun", "-c", BUILD_DIR+"libdrangry.so",
            "-file", input_file, "-log", "-verbose",
            "--", binary, input_file]

    print("running '{}'".format(args))

    env = os.environ.copy()

    env["LD_LIBRARY_PATH"] = DRMEMORY_DIR+"/drmf/lib32/release/"
    env["LD_LIBRARY_PATH"] += ":"+os.path.dirname(binary)

    native = subprocess.Popen(' '.join(args), shell=True, env=env)

    return native

def run_native_process(binary, input_file, output_dir):
    process = multiprocessing.Process(target=run_native, args=(binary, input_file, output_dir))
    process.start()


def main():
    args = docopt(__doc__, options_first=True)
    print(args)

    binary = os.path.abspath(args["<binary>"])
    output_dir = args["<output_dir>"]

    workers = int(args["--workers"])
    snapshot = args["--snapshot"] == "True"
    taint = args["--taint"] == "True"

    print("num procs: {}".format(workers))

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    infile = utils.create_random_file(output_dir, MAX_FILE_SIZE)
    run_native(binary, infile, output_dir)

    watch_manager = pyinotify.WatchManager()
    mask = pyinotify.IN_CREATE

    handler = NewTestcaseHandler(binary)

    # Start the directory watcher as a separate thread so it
    notifier = pyinotify.ThreadedNotifier(watch_manager, handler)
    watch_manager.add_watch(output_dir, mask, rec=True)

    notifier.start()

    server.start(binary, output_dir, workers, snapshot)

if __name__ == "__main__":
    main()
