#!/usr/bin/env bash

function download {
    URL=$1
    FILENAME=$2
    DOWNLOAD_DIR=$3

    if [ ! -f $FILENAME ];
    then
        echo "Downloading $FILENAME ..."
        curl -L "$URL" > "$DOWNLOAD_DIR/$FILENAME"

        BASEDIR=$(basename -s .tar.gz "$FILENAME")
        EXTRACT_DIR="$DOWNLOAD_DIR/$BASEDIR"

        rm -rf $EXTRACT_DIR
        mkdir $EXTRACT_DIR
        tar xf "$DOWNLOAD_DIR/$FILENAME" -C $EXTRACT_DIR --strip-components 1
    fi
}

DYNAMORIO_URL="https://github.com/DynamoRIO/dynamorio/releases/download/cronbuild-7.0.17837/DynamoRIO-i386-Linux-7.0.17837-0.tar.gz"
DRMEMORY_URL="https://github.com/DynamoRIO/drmemory/releases/download/cronbuild-2.0.17835/DrMemory-Linux-2.0.17835-1.tar.gz"

cd native

download $DYNAMORIO_URL dynamorio.tar.gz "prebuilts/"
download $DRMEMORY_URL drmemory.tar.gz "prebuilts/"

mkdir build
cd build

export CFLAGS=-m32 CXXFLAGS=-m32
cmake ../
make
