#include "options.h"
#include <string.h>

struct dreyfuzz_options_t drfz_opts = {
    .log = 0,
    .verbose = 0,
    .input_file = NULL
};

void parse_options(int argc, const char *argv[])
{
    for (int i=0; i<argc; i++) {
        if (strstr(argv[i], "-log")) {
            drfz_opts.log = 1;
        } else if (strstr(argv[i], "-verbose")) {
            drfz_opts.verbose = 1;
        } else if (strstr(argv[i], "-file") && argc > i+1) {
            const char* file = argv[i+1];
            if (strcmp(file, "stdin"))
                drfz_opts.input_file = argv[i+1];
        }
    }
}
