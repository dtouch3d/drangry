#include "dr_api.h"
#include "drutil.h"
#include "dr_tools.h"
#include "instrumentation.h"
#include "dreyfuzz.h"
#include "hook/libc/fs.h"
#include "utils.h"
#include "log.h"
#include "ipc.h"
#include "clean_calls.h"

#include <string.h>


shadow_value_t shadow_reg_array[MAX_REG_NUM];

int taint_hit_count1[UNTAINTED_SHADOW_VALUE_SHORT] = { 0 };
int taint_hit_count2[UNTAINTED_SHADOW_VALUE_SHORT] = { 0 };

int magic_input_cmps[UNTAINTED_SHADOW_VALUE_SHORT+1];

int* curr_hitcount = taint_hit_count1;
int* last_hitcount = taint_hit_count2;

size_t bytes_read = 0;

void shadow_memory_init(void)
{
    umbra_map_options_t ops = {
        .flags = UMBRA_MAP_CREATE_SHADOW_ON_TOUCH,
        .scale = DRFZ_UMBRA_MAP_SCALE,
        .default_value = UNTAINTED_SHADOW_VALUE,
        .default_value_size = 1
    };

    if (umbra_create_mapping(&ops, &umbra_map) != DRMF_SUCCESS)
        drfz_log("Failed to create shadow memory mapping\n");

    if (umbra_num_scratch_regs_for_translation(&umbra_num_scratch_regs) !=
            DRMF_SUCCESS)
        drfz_log("Failed to get num regs for umbra translation");

    memset(shadow_reg_array, UNTAINTED_SHADOW_VALUE, sizeof(shadow_reg_array));

    memset(magic_input_cmps, 0x55, sizeof(magic_input_cmps));
}

void shadow_memory_destroy(void)
{
    if (umbra_destroy_mapping(umbra_map) != DRMF_SUCCESS)
        drfz_log("Failed to destroy shadow memory mapping\n");
}

dr_emit_flags_t event_bb_insert(void *drcontext, void *tag, instrlist_t
        *bb, instr_t *instr, bool for_trace, bool translating, void *user_data)
{
    if (instr_in_blacklist(instr) || instr_is_meta(instr))
        return DR_EMIT_DEFAULT;

    log_disassembly_line(drcontext, instr);
    int opcode = instr_get_opcode(instr);

    opnd_t src, dst = opnd_create_null();

    /* XXX: HACK: We normalize the srcs and dsts so we can deal with
     * taint propagation in an architecture-generic manner. We remove the
     * same opnds from srcs/dsts.
     *
     * For example, here is a pop instruction in DR in x86:
     *
     * readpng: [read]  0xf3dffa48 pop    %esp (%esp)[4byte] -> %ebx %esp
     * dst: 0 reg 20,1 reg 21,
     * src: 0 reg 21, 1 [memory ref],
     *
     * Register 21 (esp) is used to compute the new esp by subtracting 4 and is
     * not relevant to us. In the end we only have the memory reference
     * [esp] that is moved to dst 0, register 20 aka ebx.
     *
     * This is the same for call, leave, ret in x86 and also conveniently
     * ignores instructions like `add eax, 0x10` since no taint propagation
     * is needed. But we have to check for something like `add eax, ebx`
     * because this would be ignored.
     */

#define MAX_OPNDS 8
    opnd_t srcs[MAX_OPNDS] = { 0 };
    opnd_t dsts[MAX_OPNDS] = { 0 };

    for (int i=0; i<MAX_OPNDS; i++)
        srcs[i] = dsts[i] = opnd_create_null();

    for (int i=0; i<instr_num_srcs(instr); i++)
        srcs[i] = instr_get_src(instr, i);

    for (int i=0; i<instr_num_dsts(instr); i++)
        dsts[i] = instr_get_dst(instr, i);

    for (int i=0; i<instr_num_srcs(instr); i++) {
        if (opnd_is_reg(srcs[i])) {
            for (int j=0; j<instr_num_dsts(instr); j++) {
                if (opnd_is_reg(dsts[j])) {
                    reg_t src_reg = opnd_get_reg(srcs[i]);
                    reg_t dst_reg = opnd_get_reg(dsts[j]);

                    if (dst_reg == src_reg) {
                        srcs[i] = dsts[j] = opnd_create_null();
                        drfz_log("x-cancelling src %d - dst %d\n", i, j);
                    }
                }
            }
        }
    }

    for (int i=0; i<MAX_OPNDS; i++) {
        if (!opnd_is_null(srcs[i])) {
            src = srcs[i];
            break;
        }
    }

    for (int i=0; i<MAX_OPNDS; i++) {
        if (!opnd_is_null(dsts[i])) {
            dst = dsts[i];
            break;
        }
    }

    if (!opnd_is_null(src) && !opnd_is_null(dst)) {
        if (opnd_is_memory_reference(src)) {
            if (opnd_is_memory_reference(dst))
                insert_memref_to_memref(drcontext, bb, instr, &src, &dst);
            else if (opnd_is_reg(dst))
                insert_memref_to_reg(drcontext, bb, instr, &src, &dst);
        } else if (opnd_is_reg(src)) {
            if (opnd_is_memory_reference(dst))
                insert_reg_to_memref(drcontext, bb, instr, &src, &dst);
            else if (opnd_is_reg(dst))
                insert_reg_to_reg(drcontext, bb, instr, &src, &dst);
        }
    } else if (instr_is_arithmetic(instr)) {
        dst = instr_get_dst(instr, 0);
        insert_reg_to_reg(drcontext, bb, instr, &src, &dst);
    } else if (instr_is_cmp(instr)) {
        opnd_t src1 = instr_get_src(instr, 0);
        opnd_t src2 = instr_get_src(instr, 1);

        if (opnd_is_memory_reference(src1))
            insert_inc_mem_hitcount(drcontext, bb, instr, &src1, &src2);

        if (opnd_is_memory_reference(src2))
            insert_inc_mem_hitcount(drcontext, bb, instr, &src2, &src1);

        if (opnd_is_reg(src1))
            insert_inc_reg_hitcount(drcontext, bb, instr, &src1, &src2);

        if (opnd_is_reg(src2))
            insert_inc_reg_hitcount(drcontext, bb, instr, &src2, &src1);
    } else if (instr_is_cbr(instr)) {
        dr_insert_cbr_instrumentation_ex(drcontext, bb, instr, clean_call_cbr,
                OPND_CREATE_INTPTR(0));
    }

    return DR_EMIT_DEFAULT;
}

void insert_inc_mem_hitcount(void* drcontext, instrlist_t* bb, instr_t* instr, opnd_t* memref, opnd_t* cmp) {
    reg_id_t reg_src_mem_addr;
    int immed = MAGIC_IGNORE;

    pre_insert_get_mem_addr(drcontext, bb, instr, memref, &reg_src_mem_addr);

    if (opnd_is_immed(*cmp))
        immed = opnd_get_immed_int(*cmp);

    drfz_log("in insert_inc_mem_hitcount for instr \n");

    dr_insert_clean_call(drcontext, bb, instr, clean_call_inc_hitcount_mem, false, 3,
            opnd_create_reg(reg_src_mem_addr),
            opnd_create_immed_uint(opnd_size_bytes(memref), OPSZ_PTR),
            opnd_create_immed_uint(immed, OPSZ_PTR));

    post_insert_get_mem_addr(drcontext, bb, instr, reg_src_mem_addr);
}

void insert_inc_reg_hitcount(void* drcontext, instrlist_t* bb, instr_t* instr, opnd_t* opnd, opnd_t* cmp) {

    int immed = MAGIC_IGNORE;

    if (opnd_is_immed(*cmp))
        immed = opnd_get_immed_int(*cmp);

    dr_insert_clean_call(drcontext, bb, instr, clean_call_inc_hitcount_reg, false, 3,
            opnd_create_immed_uint(opnd_get_reg(*opnd), OPSZ_PTR),
            opnd_create_immed_uint(opnd_size_bytes((opnd)), OPSZ_PTR),
            opnd_create_immed_uint(immed, OPSZ_PTR));
}

void insert_memref_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_src, opnd_t* opnd_dst)
{
    reg_id_t reg_dst = opnd_get_reg(*opnd_dst);
    reg_id_t reg_src_mem_addr;

    unsigned int opnd_size = sizeof(void*);

    if (instr_num_srcs(instr) && instr_num_dsts(instr))
        opnd_size = opnds_get_min_size(memref_src, opnd_dst);

    pre_insert_get_mem_addr(drcontext, bb, instr, memref_src, &reg_src_mem_addr);

    dr_insert_clean_call(drcontext, bb, instr,
            clean_call_memref_to_reg, false, 3,
            opnd_create_reg(reg_src_mem_addr),
            opnd_create_immed_uint(reg_dst, OPSZ_PTR),
            opnd_create_immed_uint(opnd_size, OPSZ_PTR));

    post_insert_get_mem_addr(drcontext, bb, instr, reg_src_mem_addr);
}

void insert_reg_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_src, opnd_t* memref_dst)
{
    reg_id_t reg_src = opnd_get_reg(*opnd_src);
    reg_id_t reg_dst_mem_addr;

    unsigned int opnd_size = sizeof(void*);

    if (instr_num_srcs(instr) && instr_num_dsts(instr))
        opnd_size = opnds_get_min_size(memref_dst, opnd_src);

    pre_insert_get_mem_addr(drcontext, bb, instr, memref_dst, &reg_dst_mem_addr);

    dr_insert_clean_call(drcontext, bb, instr,
            clean_call_reg_to_memref, false, 3,
            opnd_create_reg(reg_dst_mem_addr),
            opnd_create_immed_uint(get_canonicalized_reg(reg_src), OPSZ_PTR),
            opnd_create_immed_int(opnd_size, OPSZ_PTR));

    post_insert_get_mem_addr(drcontext, bb, instr, reg_dst_mem_addr);
}

void insert_memref_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_src, opnd_t* memref_dst)
{
    reg_id_t reg_src_mem_addr;
    reg_id_t reg_dst_mem_addr;

    unsigned int opnd_size = sizeof(void*);

    if (instr_num_srcs(instr) && instr_num_dsts(instr))
        opnd_size = opnds_get_min_size(memref_dst, memref_src);

    pre_insert_get_mem_addr(drcontext, bb, instr, memref_dst, &reg_dst_mem_addr);
    pre_insert_get_mem_addr(drcontext, bb, instr, memref_src, &reg_src_mem_addr);

    dr_insert_clean_call(drcontext, bb, instr,
            clean_call_memref_to_memref, false, 3,
            opnd_create_reg(reg_dst_mem_addr),
            opnd_create_reg(reg_src_mem_addr),
            opnd_create_immed_uint(opnd_size, OPSZ_PTR));

    post_insert_get_mem_addr(drcontext, bb, instr, reg_dst_mem_addr);
    post_insert_get_mem_addr(drcontext, bb, instr, reg_src_mem_addr);
}

void insert_reg_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_src, opnd_t* opnd_dst)
{
    dr_insert_clean_call(drcontext, bb, instr,
            clean_call_reg_to_reg, false, 2,
            opnd_create_immed_uint(
                get_canonicalized_reg(opnd_get_reg(*opnd_src)), OPSZ_PTR),
            opnd_create_immed_uint(
                get_canonicalized_reg(opnd_get_reg(*opnd_dst)), OPSZ_PTR));
}

void insert_immed_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_dst)
{
    reg_id_t reg_dst_mem_addr;

    unsigned int opnd_size = opnd_get_size(*memref_dst);

    pre_insert_get_mem_addr(drcontext, bb, instr, memref_dst, &reg_dst_mem_addr);

    dr_insert_clean_call(drcontext, bb, instr,
        clean_call_imm_to_memref, false, 2,
            opnd_create_reg(reg_dst_mem_addr),
            opnd_create_immed_uint(opnd_size, OPSZ_PTR));

    post_insert_get_mem_addr(drcontext, bb, instr, reg_dst_mem_addr);
}

void insert_immed_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_dst)
{
    dr_insert_clean_call(drcontext, bb, instr,
        clean_call_imm_to_reg, false, 1,
        opnd_create_immed_uint(
            get_canonicalized_reg(opnd_get_reg(*opnd_dst)), OPSZ_PTR));
}

void handle_mov_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst)
{

    if (opnd_is_memory_reference(*src) && opnd_is_reg(*dst)) {
        /* XXX i#12: check if src memref uses tainted regs */
        insert_memref_to_reg(drcontext, bb, instr, src, dst);
        return;
    }

    if (opnd_is_memory_reference(*dst) && opnd_is_reg(*src)) {
        /* check if dst memref uses tainted regs */
        insert_reg_to_memref(drcontext, bb, instr, src, dst);
        return;
    }

    if (opnd_is_reg(*src) && opnd_is_reg(*dst)) {
        /* copy taint form reg src to reg dst */
        insert_reg_to_reg(drcontext, bb, instr, src, dst);
        return;
    }

    if (opnd_is_reg(*dst) && opnd_is_immed(*src)) {
        insert_immed_to_reg(drcontext, bb, instr, dst);
        return;
    }

}

void handle_arithmetic_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst)
{
    int opcode = instr_get_opcode(instr);

    if (opnd_is_reg(*src) && opnd_is_reg(*dst)) {
        reg_id_t reg_src = opnd_get_reg(*src);
        reg_id_t reg_dst = opnd_get_reg(*dst);

        if (reg_src == reg_dst && (opcode == OP_xor
            || opcode == OP_sub || opcode == OP_sbb)) {

            insert_immed_to_reg(drcontext, bb, instr, dst);

        } else {
            insert_reg_to_reg(drcontext, bb, instr, src, dst);
        }

        return;
    }
}

void handle_lea_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst)
{
        int num_regs_used = opnd_num_regs_used(*src);

#define LEA_MAX_REGS_USED 4

        if (num_regs_used > LEA_MAX_REGS_USED)
            drfz_log("lea called with %d registers used!\n", num_regs_used);

        reg_id_t regs_used[LEA_MAX_REGS_USED] = { 0 };

        for (int i=0; i<num_regs_used; i++) {
            regs_used[i] = opnd_get_reg_used(*src, i);
        }

        dr_insert_clean_call(drcontext, bb, instr,
            clean_call_multireg_to_reg, false, LEA_MAX_REGS_USED+1,
            opnd_create_immed_uint(
                get_canonicalized_reg(opnd_get_reg(*dst)), OPSZ_PTR),
            opnd_create_immed_uint(
                get_canonicalized_reg(regs_used[0]), OPSZ_PTR),
             opnd_create_immed_uint(
                get_canonicalized_reg(regs_used[1]), OPSZ_PTR),
             opnd_create_immed_uint(
                get_canonicalized_reg(regs_used[2]), OPSZ_PTR),
             opnd_create_immed_uint(
                get_canonicalized_reg(regs_used[3]), OPSZ_PTR));
}
