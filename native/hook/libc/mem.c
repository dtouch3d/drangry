#include "dr_api.h"
#include "drwrap.h"
#include "log.h"
#include "clean_calls.h"
#include "instrumentation.h"

void pre_memcmp(void* wrapcxt, void** user_data) {
    void* ptr1 = (void*)drwrap_get_arg(wrapcxt, 0);
    void* ptr2 = (void*)drwrap_get_arg(wrapcxt, 1);
    size_t size = (size_t)drwrap_get_arg(wrapcxt, 2);

    drfz_printf("memcmp(%p, %p, %zu)\n", ptr1, ptr2, size);

    for (int i=0; i<size; i++) {
        clean_call_inc_hitcount_mem(ptr1+i, 1, MAGIC_IGNORE);
        clean_call_inc_hitcount_mem(ptr2+i, 1, MAGIC_IGNORE);
    }
}
