#include "dr_api.h"
#include "drwrap.h"
#include "utils.h"
#include "options.h"
#include "instrumentation.h"
#include "log.h"
#include "ipc.h"
#include "pmparser.h"
#include "hook/libc/fs.h"
#include "hook/hook.h"

#include <string.h>
#include <sys/stat.h>
#include <stdint.h>
#include <libgen.h>

char* input_filepath;
int input_fd = 0;
void* input_file_ptr = 0;
size_t already_read = 0;

void
pre_open(void *wrapcxt, void** user_data)
{
    bool ret;
    char* path = drwrap_get_arg(wrapcxt, 0);
    drfz_log("Called open(%s)\n", path);

    *user_data = 0;

    if (drfz_opts.input_file) {
        if (strstr(path, drfz_opts.input_file)) {
            input_filepath = strdup(path);
            *user_data = input_filepath;
        }
    }
}

void
post_open(void *wrapcxt, void** user_data)
{
    if (!user_data)
        return;

    int enable = *(int*)user_data;

    if (enable) {
        input_fd = (int)drwrap_get_retval(wrapcxt);
        drfz_log("Input file %s, fd %d\n", input_filepath, input_fd);

        unhook("fopen");
        unhook("fread");
    }

}

void
pre_fopen(void *wrapcxt, void** user_data)
{
    bool ret;
    char* path = drwrap_get_arg(wrapcxt, 0);
    drfz_log("Called fopen(%s)\n", path);

    *user_data = 0;

    if (drfz_opts.input_file) {
        if (strstr(path, drfz_opts.input_file)) {
            input_filepath = strdup(path);
            *user_data = input_filepath;

            unhook("open");
            unhook("read");
        }
    }
}

void
post_fopen(void *wrapcxt, void** user_data)
{
    if (!user_data)
        return;

    int enable = *(int*)user_data;

    if (enable) {
        input_file_ptr = (void*)drwrap_get_retval(wrapcxt);
        drfz_log("Input file %s, fd %d\n", input_filepath, input_file_ptr);
    }

}

void
pre_read(void *wrapcxt, void** user_data)
{
    int ret;
    int status;
    long retaddr;
    int fd = (int)drwrap_get_arg(wrapcxt, 0);
    drfz_log("pre_read fd: %d\n", fd);

    if (fd != input_fd) {
        drfz_log("input_fd: %d, exiting ... ", input_fd);
        *user_data = 0;
        return;
    }

    void* buf = drwrap_get_arg(wrapcxt, 1);
    size_t n = (size_t)drwrap_get_arg(wrapcxt, 2);
    *user_data = buf;

    drfz_log("Called read(%d, %p, %d)\n", fd, buf, n);
    pre_input_send(wrapcxt, buf, n);
}

void
pre_fread(void *wrapcxt, void** user_data)
{
    int ret;
    int status;
    long retaddr;
    void* file_ptr = drwrap_get_arg(wrapcxt, 3);

    if (file_ptr != input_file_ptr) {
        *user_data = 0;
        return;
    }

    void* buf = drwrap_get_arg(wrapcxt, 0);
    size_t nmemb = (size_t)drwrap_get_arg(wrapcxt, 1);
    size_t size = (size_t)drwrap_get_arg(wrapcxt, 2);

    size_t nbytes = nmemb*size;

    *user_data = buf;

    drfz_log("Called fread(%p, %zu, %zu, %p)\n", buf, nmemb, size, file_ptr);

    pre_input_send(wrapcxt, buf, nbytes);
}


int pre_input_send(void* wrapcxt, void* buf, size_t size) {

    /* For every call to read that is interesting, we create a new connection
     * to angr so we can distinguish between multiple reads of the input. We
     * just have to send the bytes we have already read in init_shadow_taint().
     * The new connection is handled automatically by the other side.
     */

    initialize_ipc();
    send_msg(INPUT_FILE_CMD, input_filepath, strlen(input_filepath));

    if (first_run == 0)
        return 0;

    void* retaddr = drwrap_get_retaddr(wrapcxt);

    send_msg(SEND_READ_RETADDR_CMD, &retaddr, sizeof(retaddr));

    send_app_state(wrapcxt);

    dr_mcontext_t* mcontext = drwrap_get_mcontext(wrapcxt);
    send_state_regs(mcontext);
}

void
post_read(void *wrapcxt, void** user_data)
{
    int ret;
    void* buf = user_data;

    bytes_read = (ssize_t)drwrap_get_retval(wrapcxt);

    init_shadow_taint(buf, bytes_read);

}

void
post_fread(void *wrapcxt, void** user_data)
{
    int ret;
    void* buf = user_data;

    bytes_read = (ssize_t)drwrap_get_retval(wrapcxt);

    init_shadow_taint(buf, bytes_read);

}

int init_shadow_taint(void* buf, size_t size) {
    int ret = umbra_create_shadow_memory(umbra_map, 0, buf, size,
            UNTAINTED_SHADOW_VALUE, 1);

    drfz_log("creating shadow mem for addr %p, size %zu\n", buf, size);

    if (ret != DRMF_SUCCESS) {
        drfz_log("could not create shadow memory for address %p : ret %d\n", buf, ret);
        return ret;
    }

    umbra_shadow_memory_info_t info = { 0 };
    info.struct_size = sizeof(umbra_shadow_memory_info_t);

    int16_t* shadow_addr;
    ret = umbra_get_shadow_memory(umbra_map, buf, (byte**)&shadow_addr, &info);

    if (ret != DRMF_SUCCESS) {
        drfz_log("could not get shadow memory for app address %p: %d\n", buf, ret);
        return ret;
    }

    drfz_log("Got shadow memory %p for app address %p\n", shadow_addr, buf);

    /* Initialize shadow memory with the index of the
     * memory contents in the original input
     */

    send_msg(FILE_OFFSET_CMD, &already_read, sizeof(already_read));

    for (size_t i=already_read; i<size+already_read; i++) {
        shadow_addr[i] = i;
    }

    already_read += size;

    /*if (size <= 0) {*/
        /*drfz_log("read() returned %d!\n", size);*/
        /*return;*/
    /*}*/
    send_msg(INPUT_BUFFER_CMD, &buf, sizeof(&buf));
    return DRMF_SUCCESS;
}

void send_app_state(void* wrapcxt) {
    procmaps_struct* procmap_tmp;
    procmaps_struct* procmap = pmparser_parse(-1);

    if (!procmap) {
        drfz_log("Could not parse /proc/self/maps!");
        return;
    }

    /* Send application state, registers and memory */
    while ((procmap_tmp = pmparser_next()) != NULL) {

        if (procmap_tmp->is_r && !procmap_tmp->is_x
            && !dr_memory_is_dr_internal(procmap_tmp->addr_start)
            && !dr_memory_is_in_client(procmap_tmp->addr_start)

            /* Weird things happen when we try to access vdso
             * so we ignore it
             */
            && !strstr(procmap_tmp->pathname, "vvar")) {

                send_memory(procmap_tmp);
        }
    }

    pmparser_free(procmap);
}

void
pre_close(void *wrapcxt, void** user_data)
{
    bool ret;
    int fd = (int)drwrap_get_arg(wrapcxt, 0);

    drfz_log("Called close(%d)\n", fd);

}
