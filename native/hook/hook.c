#include "hook/hook.h"
#include "dr_api.h"
#include "drwrap.h"
#include "utils.h"
#include "log.h"
#include "instrumentation.h"
#include "clean_calls.h"
#include "hook/libc/fs.h"
#include "hook/libc/mem.h"
#include <string.h>


/* XXX: This table needs to be more complete as there is a myriad of functions
 * that take file descriptors for arguments. For instance, the very simple "cat"
 * program returns a "Bad file descriptor error" because it tries to __fxstat
 * or posix_fadvise our file descriptor
 */

static struct module_info hook_table[] = {
    {
        .name = "libc",
        .functions = {
            { "__libc_start_main", NULL, pre_libc_start_main, NULL},
            { "open",   NULL, pre_open, post_open},
            { "fopen",  NULL, pre_fopen, post_fopen},
            { "open64", NULL, pre_open, post_open},
            { "close",  NULL, pre_close, NULL},
            { "read",   NULL, pre_read, post_read },
            { "fread",  NULL, pre_fread, post_fread },
            { "memcmp",  NULL, pre_memcmp, NULL },
            { 0 },
        }
    }
};
void unhook(char* func) {
    int ret;

    for (int i=0; i<ARRAY_SIZE(hook_table); i++) {
        struct function_info* function_list = hook_table[i].functions;

        for (int j=0; j<MAX_FUNCTIONS && function_list[j].name != NULL; j++) {
            struct function_info function = function_list[j];
            drfz_printf("checking %s with %s\n", function.name, func);

            if (strcmp(function.name, func) == 0) {
                ret = drwrap_unwrap(function.addr, function.pre_func, function.post_func);
                drfz_printf("unhooking %s ret: %d\n", func, ret);
                return;
            }

        }

    }
}

void
event_module_load(void *drcontext, const module_data_t *info, bool loaded)
{
    const char* module_name = dr_module_preferred_name(info);

    for(int i=0; i<ARRAY_SIZE(hook_table); i++) {
        if (strstr(module_name, hook_table[i].name) != NULL) {
            drfz_log("Found %s\n", module_name);

            struct function_info* function_list = hook_table[i].functions;

            for(int j=0; j<MAX_FUNCTIONS && function_list[j].name != NULL; j++) {
                void* addr = dr_get_proc_address(info->handle, function_list[j].name);

                if (addr != NULL) {
                    struct function_info* function = &function_list[j];

                    function->addr = addr;

                    if (function->pre_func) {
                        drfz_log("Hooking %s() in %s @ %p\n", function->name, module_name, addr);
                        drwrap_wrap((app_pc) addr, function->pre_func, function->post_func);
                    }
                }
            }

            break;
        }
    }
}

void* main_addr;
void* main_stack_ptr;
void* min_stack;
#define MAX_STACK_SIZE 8*SZ_1MB

void
pre_main_shadow_stack_init(void* wrapcxt, void** userdata)
{
    dr_mcontext_t* mcontext = drwrap_get_mcontext(wrapcxt);
    drfz_log("Got XSP %p on main\n", mcontext->xsp);

    min_stack = (char*)mcontext->xsp - MAX_STACK_SIZE;

    int ret = umbra_create_shadow_memory(umbra_map, 0, min_stack, MAX_STACK_SIZE,
            UNTAINTED_SHADOW_VALUE, 1);

    if (ret != DRMF_SUCCESS) {
        drfz_log("Could not create shadow memory for stack! : %d\n", ret);
    }
}

void
pre_libc_start_main(void* wrapcxt, void** userdata)
{
    main_addr = drwrap_get_arg(wrapcxt, 0);
    drfz_log("Got main address %p\n", main_addr);

    drwrap_wrap((app_pc)main_addr, pre_main_shadow_stack_init, NULL);
}

void
pre_skip_call_success(void* wrapcxt, void** userdata)
{
    bool ret = drwrap_skip_call(wrapcxt, 0, 0);
    void* addr = drwrap_get_func(wrapcxt);

    drfz_log("called %p\n", addr);

    if (ret)
        drfz_log("Failed to skip call %x\n", drwrap_get_func(wrapcxt));
}
