#include "utils.h"
#include "log.h"
#include "options.h"
#include <string.h>
#include <libgen.h>

const char* get_module_name_from_instr(instr_t* instr)
{
    byte* pc = instr_get_app_pc(instr);

    module_data_t* current_module = dr_lookup_module(pc);
    return dr_module_preferred_name(current_module);
}

/* We only want to instrument the relevant applications code, so we
 * blacklist ld-linux.so. We want to have libc instrumented
 * and maybe fastpath for common functions.
 */

const char* taint_module_blacklist[] = {
    "ld-linux.so",
    "libc.so",
    "libc.so.6",
};

bool instr_in_blacklist(instr_t* instr)
{
    const char* module_name = get_module_name_from_instr(instr);
    /*drfz_printf("module name for pc %p: %s\n", instr_get_app_pc(instr), module_name);*/

    if (!module_name)
        return true;

    for (int i = 0; i < ARRAY_SIZE(taint_module_blacklist); i++) {
        if (!strncmp(module_name, taint_module_blacklist[i],
                strlen(taint_module_blacklist[i]))) {
            return true;
        }
    }

    return false;
}

const char* instr_to_memop(instr_t *instr)
{
    char* mem_op;

    if (instr_reads_memory(instr) && !instr_writes_memory(instr)) {
        mem_op = "read";
    } else if (instr_writes_memory(instr) && !instr_reads_memory(instr)) {
        mem_op = "write";
    } else if (instr_reads_memory(instr) && instr_writes_memory(instr)) {
        mem_op = "read/write";
    } else {
        mem_op = "other";
    }

    return mem_op;
}

unsigned int opnds_get_min_size(opnd_t* opnd1, opnd_t* opnd2)
{
    unsigned int opnd_sz1 = opnd_size_bytes(opnd1);
    unsigned int opnd_sz2 = opnd_size_bytes(opnd2);

    if (opnd_sz1 < opnd_sz2)
        return opnd_sz1;

    return opnd_sz2;
}

unsigned int opnd_size_bytes(opnd_t* opnd)
{
    return opnd_size_in_bytes(opnd_get_size(*opnd));
}

void log_disassembly_line(void* drcontext, instr_t* instr)
{
    if (!drfz_opts.verbose)
        return;

    byte* pc = instr_get_app_pc(instr);

    drfz_log("%s: [%s]", get_module_name_from_instr(instr), instr_to_memop(instr));

    disassemble(drcontext, pc, logfd);

    if (instr_num_dsts(instr))
        drfz_log("dst: ");

    for (int i = 0; i < instr_num_dsts(instr); i++) {
        opnd_t dst_opnd = instr_get_dst(instr, i);

        if (opnd_is_memory_reference(dst_opnd)) {
            drfz_log("%d [memory ref],", i);
        } else if (opnd_is_reg(dst_opnd)) {
            reg_id_t reg = opnd_get_reg(dst_opnd);
            drfz_log("%d reg %s,", i, get_register_name(reg));
        } else if (opnd_is_immed(dst_opnd)) {
            int immed = opnd_get_immed_int(dst_opnd);
            drfz_log("%d immed %x, ", i, immed);
        }
    }

    if (instr_num_dsts(instr))
        drfz_log("\n");

    if (instr_num_srcs(instr))
        drfz_log("src: ");

    for (int i = 0; i < instr_num_srcs(instr); i++) {
        opnd_t src_opnd = instr_get_src(instr, i);

        if (opnd_is_memory_reference(src_opnd)) {
            drfz_log("%d [memory ref], ", i);
        } else if (opnd_is_reg(src_opnd)) {
            reg_id_t reg = opnd_get_reg(src_opnd);
            drfz_log("%d reg %d, ", i, get_register_name(reg));
        } else if (opnd_is_immed(src_opnd)) {
            int immed = opnd_get_immed_int(src_opnd);
            drfz_log("%d immed %x, ", i, immed);
        }
    }

    if (instr_num_srcs(instr))
        drfz_log("\n");
}

/* We add this because movzx is not considered a "mov" instruction by dynamorio
 * https://github.com/DynamoRIO/dynamorio/pull/2643
 */
bool instr_is_mov_or_movzx(instr_t* instr)
{
    return instr_is_mov(instr) || (instr_get_opcode(instr) == OP_movzx);
}

bool instr_is_cmp(instr_t* instr)
{
    int opcode = instr_get_opcode(instr);

    return opcode == OP_cmp;
}

bool instr_is_arithmetic(instr_t* instr)
{
    int opcode = instr_get_opcode(instr);

    return opcode == OP_add || opcode == OP_adc
        || opcode == OP_sub || opcode == OP_sbb
        || opcode == OP_inc || opcode == OP_dec
        || opcode == OP_mul || opcode == OP_imul
        || opcode == OP_div || opcode == OP_idiv
        || opcode == OP_and || opcode == OP_or
        || opcode == OP_not || opcode == OP_neg
        || opcode == OP_shr || opcode == OP_shl
        || opcode == OP_xor;
}

reg_id_t get_canonicalized_reg(reg_id_t reg) {
    switch(reg) {
        case DR_REG_RAX:
        case DR_REG_EAX:
        case DR_REG_AX:
        case DR_REG_AL:
        case DR_REG_AH:
            return DR_REG_XAX;
            break;
        case DR_REG_RBX:
        case DR_REG_EBX:
        case DR_REG_BX:
        case DR_REG_BL:
        case DR_REG_BH:
            return DR_REG_XBX;
            break;
        case DR_REG_RCX:
        case DR_REG_ECX:
        case DR_REG_CX:
        case DR_REG_CL:
        case DR_REG_CH:
            return DR_REG_XCX;
            break;
        case DR_REG_RDX:
        case DR_REG_EDX:
        case DR_REG_DX:
        case DR_REG_DL:
        case DR_REG_DH:
            return DR_REG_XDX;
            break;
        case DR_REG_RSI:
        case DR_REG_ESI:
        case DR_REG_SI:
            return DR_REG_XSI;
            break;
        case DR_REG_RDI:
        case DR_REG_EDI:
        case DR_REG_DI:
            return DR_REG_XDI;
            break;
        default:
            return reg;
    }
}

void print_mcontext(dr_mcontext_t *mc)
{
    dr_printf("\txax=%p, xbx=%p, xcx=%p, xdx=%p\n"
              "\txsi=%p, xdi=%p, xbp=%p, xsp=%p\n"
              "\txip=%p",
              mc->xax, mc->xbx, mc->xcx, mc->xdx,
              mc->xsi, mc->xdi, mc->xbp, mc->xsp,
              mc->pc);
}

void hexdump(const void* data, size_t size)
{
    char ascii[17];
    size_t i, j;
    ascii[16] = '\0';
    for (i = 0; i < size; ++i) {
        dr_printf("%02X ", ((unsigned char*)data)[i]);
        if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
            ascii[i % 16] = ((unsigned char*)data)[i];
        } else {
            ascii[i % 16] = '.';
        }
        if ((i+1) % 8 == 0 || i+1 == size) {
            dr_printf(" ");
            if ((i+1) % 16 == 0) {
                dr_printf("|  %s \n", ascii);
            } else if (i+1 == size) {
                ascii[(i+1) % 16] = '\0';
                if ((i+1) % 16 <= 8) {
                    dr_printf(" ");
                }
                for (j = (i+1) % 16; j < 16; ++j) {
                    dr_printf("   ");
                }
                dr_printf("|  %s \n", ascii);
            }
        }
    }
}
