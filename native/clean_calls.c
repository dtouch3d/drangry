#include "dr_api.h"
#include "drutil.h"
#include "umbra.h"
#include "clean_calls.h"
#include "instrumentation.h"
#include "log.h"
#include "ipc.h"
#include "hook/libc/fs.h"

void pre_insert_get_mem_addr(void *drcontext, instrlist_t *ilist, instr_t *where,
        opnd_t *memref, reg_id_t *reg_addr)
{
    int ok;
    reg_id_t scratch;

    /* Steal two scratch registers */

    if (drreg_reserve_register(drcontext, ilist, where, NULL, reg_addr) !=
            DRREG_SUCCESS ||
         drreg_reserve_register(drcontext, ilist, where, NULL, &scratch) !=
            DRREG_SUCCESS) {

        DR_ASSERT_MSG(false, "Could not get scratch registers for instrumentation");
        return;
    }

    ok = drutil_insert_get_mem_addr(drcontext, ilist, where, *memref, *reg_addr, scratch);

    DR_ASSERT_MSG(ok, "failed to insert get mem addr");

    if (drreg_unreserve_register(drcontext, ilist, where, scratch) !=
            DRREG_SUCCESS) {

        DR_ASSERT_MSG(false, "Could not release scratch registers");
        return;
    }
}

void post_insert_get_mem_addr(void *drcontext, instrlist_t *ilist, instr_t *where, reg_id_t reg_addr)
{
    if (drreg_unreserve_register(drcontext, ilist, where, reg_addr) !=
            DRREG_SUCCESS) {

        DR_ASSERT_MSG(false, "Could not release scratch registers");
        return;
    }
}


void print_all_shadow_mem(void* pc)
{
    umbra_iterate_shadow_memory(umbra_map, NULL, iter_print_shadow_mem);
}

bool iter_print_shadow_mem(umbra_map_t *map, umbra_shadow_memory_info_t *info, void *user_data)
{
    drfz_log("helloooo from iter_print_shadow_mem\n");
    return true;
}

void clean_call_memref_to_reg(void* src_mem_addr, unsigned long reg_dst, unsigned int size)
{
    int ret;
    shadow_value_t shadow_val;
    /* XXX: This could get ugly with a larger granularity */
    size_t val_size = sizeof(shadow_val)*size;

    drfz_log("%s\n", __func__);
    drfz_log_ptr(reg_dst);
    drfz_log_ptr(src_mem_addr);
    drfz_log_ptr(size);

    ret = umbra_read_shadow_memory(umbra_map, src_mem_addr,
            size, &val_size, (byte*)&shadow_val);

    if (ret != DRMF_SUCCESS) {
        drfz_log("Got ret: %d for umbra_read_shadow_mem\n");
        return;
    }

    drfz_log("Propagated shadow val %d to reg %d from %p\n", shadow_val,
            reg_dst, src_mem_addr);

    shadow_reg_array[reg_dst] = shadow_val;
    curr_hitcount[shadow_val]++;
    return;
}

void clean_call_reg_to_memref(void* dst_mem_addr, unsigned long reg_src,
        unsigned int size)
{
    int ret;
    shadow_value_t shadow_val;
    size_t shadow_size = sizeof(shadow_val)*size;

    drfz_log("%s\n", __func__);
    drfz_log_ptr(size);
    drfz_log_ptr(shadow_size);
    shadow_val = shadow_reg_array[reg_src];

    ret = umbra_write_shadow_memory(umbra_map, dst_mem_addr,
            size, &shadow_size, (byte*)&shadow_val);

    if (ret != DRMF_SUCCESS) {
        drfz_log("Got ret: %d on umbra_write_shadow_mem\n", ret);
        return;
    }

    drfz_log("Propagated shadow val %d from reg %d to %p\n", shadow_val,
            reg_src, dst_mem_addr);
    curr_hitcount[shadow_val]++;

    return;
}

void clean_call_memref_to_memref(void* dst_mem_addr, void* src_mem_addr, unsigned int size)
{
    int ret;
    shadow_value_t shadow_val_src;
    size_t shadow_size = sizeof(shadow_val_src)*size;

    drfz_log("%s\n", __func__);
    drfz_log_ptr(dst_mem_addr);
    drfz_log_ptr(src_mem_addr);
    drfz_log_ptr(size);

    ret = umbra_read_shadow_memory(umbra_map, src_mem_addr,
            size, &shadow_size, (byte*)&shadow_val_src);

    if (ret != DRMF_SUCCESS) {
        drfz_log("Got ret: %d for umbra_read_shadow_mem src memory address\n");
        return;
    }

    if (shadow_val_src) {
        ret = umbra_write_shadow_memory(umbra_map, dst_mem_addr,
                size, &shadow_size, (byte*)&shadow_val_src);

        if (ret != DRMF_SUCCESS) {
            drfz_log("Got ret: %d for umbra_read_shadow_mem dst memory address\n");
            return;
        }

        drfz_log("Propagated shadow val %d to %p from %p\n", shadow_val_src,
                dst_mem_addr, src_mem_addr);
        curr_hitcount[shadow_val_src]++;
    }
}

void clean_call_imm_to_reg(int reg_dst)
{
    drfz_log("%s\n", __func__);
    drfz_log("Removing taint from register %d\n", reg_dst);
    shadow_reg_array[reg_dst] = UNTAINTED_SHADOW_VALUE_SHORT;
}

void clean_call_imm_to_memref(void* dst_mem_addr,
        unsigned int size)
{
    int ret;
    shadow_value_t shadow_val;
    size_t shadow_size = size;

    drfz_log("%s\n", __func__);
    drfz_log_ptr(size);
    drfz_log_ptr(shadow_size);
    shadow_val = UNTAINTED_SHADOW_VALUE_SHORT;

    ret = umbra_write_shadow_memory(umbra_map, dst_mem_addr,
            size, &shadow_size, (byte*)&shadow_val);

    if (ret != DRMF_SUCCESS) {
        drfz_log("Got ret: %d on umbra_write_shadow_mem\n", ret);
        return;
    }

    drfz_log("Removing taint from %p\n", dst_mem_addr);

    return;
}

void clean_call_reg_to_reg(int reg_src, int reg_dst)
{
    drfz_log("%s\n", __func__);
    shadow_value_t shadow_val;

    shadow_val = shadow_reg_array[reg_src];
    shadow_reg_array[reg_dst] = shadow_val;

    drfz_log("Propagated shadow val %d to reg %d from %d\n", shadow_val, reg_dst, reg_src);
    curr_hitcount[shadow_val]++;
}

void clean_call_multireg_to_reg(int reg_dst, int reg1, int reg2, int reg3,
        int reg4)
{
    drfz_log("%s\n", __func__);
    if (reg1 != 0)
        clean_call_reg_to_reg(reg1, reg_dst);
    if (reg2 != 0)
        clean_call_reg_to_reg(reg2, reg_dst);
    if (reg3 != 0)
        clean_call_reg_to_reg(reg3, reg_dst);
    if (reg4 != 0)
        clean_call_reg_to_reg(reg4, reg_dst);
    drfz_log("%s end\n", __func__);
}

void clean_call_cbr(app_pc instr_addr, app_pc target_addr, app_pc fall_addr, int taken, void* dummy) {
    void* tmp;

    if (already_read == 0)
        return;

    struct cbr_msg_t cbr_msg = {
        .instr_addr = instr_addr,
        .target_addr = target_addr,
        .fall_addr = fall_addr,
        .taken = taken
    };

    send_msg(SEND_CBR_CMD, &cbr_msg, sizeof(cbr_msg));
    drfz_printf("cbr @ instr_addr: %p, target: %p, fallback: %p\n", instr_addr, target_addr, fall_addr);
    send_msg(SEND_TAINT_HIT_COUNT_CMD, curr_hitcount, sizeof(curr_hitcount[0])*already_read);
    send_msg(SEND_MAGIC_INPUT_CMPS, magic_input_cmps, sizeof(magic_input_cmps[0])*already_read);

}

void clean_call_inc_hitcount_mem(void* mem, unsigned int size, int immed) {
    int ret;
    shadow_value_t shadow_val = 0;

    size_t val_size = sizeof(shadow_val)*size;

    ret = umbra_read_shadow_memory(umbra_map, mem,
            size, &val_size, (byte*)&shadow_val);

    if (ret == DRMF_SUCCESS) {

        if (immed != MAGIC_IGNORE)
            drfz_printf("checking byte %d with %d\n", shadow_val, immed);

        magic_input_cmps[shadow_val] = immed;
        curr_hitcount[shadow_val] += 1;
    }

}


void clean_call_inc_hitcount_reg(reg_t reg, unsigned int size, int immed) {

    int index = shadow_reg_array[reg];

    if (immed != MAGIC_IGNORE)
        drfz_printf("checking byte %d with %d\n", index, immed);

    magic_input_cmps[index] = immed;
    curr_hitcount[index] += 1;
}
