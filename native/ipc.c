#include <errno.h>
#include <unistd.h>

#include "ipc.h"
#include "log.h"
#include "pmparser.h"

int first_run = 1;

int initialize_ipc(void) {
    int ret = -1;
    ssize_t sent;
    int sock;
    ssize_t received;

    drfz_log("initializing ipc\n");
    if (client_sock)
        cleanup_ipc();
    struct sockaddr_un client_sockaddr;
    memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));

    sock = socket(AF_UNIX, SOCK_STREAM, 0);

    if (sock == -1) {
        drfz_printf("Could not initialize domain socket");
        return -1;
    }

    client_sockaddr.sun_family = AF_UNIX;
    strcpy(client_sockaddr.sun_path, "/tmp/dreyfuzz.sock");

    for (int i=0; i<16; i++) {
        ret = connect(sock, (struct sockaddr*)&client_sockaddr, sizeof(client_sockaddr));

        if (ret) {
            drfz_printf("Could not connect to socket: %d, errno: %d, retrying ...\n", ret, errno);
            usleep(1000);
            continue;
        } else {
            client_sock = sock;
            break;
        }
    }

    received = recv(client_sock, &first_run, sizeof(first_run), 0);
    drfz_printf("received %lu\n", received);

    drfz_printf("initialize_ipc first run: %d\n", first_run);

    module_data_t* main_module = dr_get_main_module();
    drfz_printf("main module start @ %p\n", main_module->start);

    if (!main_module) {
        drfz_printf("Could not get main module info!");
    }

    struct init_msg_t init_msg = {
        .main_module_start = main_module->start
    };


    if (first_run == 0)
        return 0;

    procmaps_struct* procmap_tmp;
    procmaps_struct* procmap = pmparser_parse(-1);

    if (!procmap) {
        drfz_log("Could not parse /proc/self/maps!");
        return -1;
    }

    while ((procmap_tmp = pmparser_next()) != NULL) {

        if (procmap_tmp->pathname && procmap_tmp->is_x) {
           if (strstr(procmap_tmp->pathname, "lib") != NULL
               && strstr(procmap_tmp->pathname, "dynamorio") == NULL
               && strstr(procmap_tmp->pathname, "drmemory")  == NULL
               && strstr(procmap_tmp->pathname, "libc")  == NULL
               && strstr(procmap_tmp->pathname, "ld-")  == NULL
               && strstr(procmap_tmp->pathname, "libdrangry.so")  == NULL) {

               struct lib_msg_t msg = {
                   .addr_start = procmap_tmp->addr_start,
                   .name = { 0 }
               };
               strncpy(msg.name, procmap_tmp->pathname, sizeof(msg.name));
               send_msg(SEND_LIB_INFO_CMD, &msg, sizeof(msg.addr_start)+strlen(msg.name));
           }
        }
    }
    pmparser_free(procmap_tmp);

    sent = send_msg(INITIALIZE_CMD, &init_msg, sizeof(init_msg));

    return 0;
}

int cleanup_ipc(void) {
    drfz_log("closing socket\n");
    close(client_sock);
    client_sock = 0;
}

/* We use the netstring protocol supported by Twisted so we don't have to deal
 * with data buffering and complex state machines.
 */

static char comma = ',';

int send_msg_with_header(enum drfz_cmd_t cmd, void* header, size_t header_len, void* msg, size_t msg_len) {

    ssize_t sent = 0;
    char prefix[32] = { 0 };
    size_t total_length = header_len+msg_len;

    if (client_sock == 0 || total_length == 0)
        return 0;

    snprintf(prefix, sizeof(prefix), "%d", total_length+sizeof(cmd));
    prefix[strlen(prefix)] = ':';

    sent += send(client_sock, prefix, strlen(prefix), 0);

    if (sent < 0)
        perror("sending in socket");

    sent += send(client_sock, &cmd, sizeof(cmd), 0);

    if (header)
        sent += send(client_sock, header, header_len, 0);

    if (msg)
        sent += send(client_sock, msg, msg_len, 0);

    sent += send(client_sock, &comma, sizeof(comma), 0);

    /*if (sent != (strlen(prefix)+sizeof(cmd)+len+sizeof(comma))) {*/
        drfz_log("msg %d, size %d sent %lu bytes instead of %lu\n", cmd, total_length, sent,
                (strlen(prefix)+sizeof(cmd)+total_length+sizeof(comma)));
    /*}*/

    return sent;
}

int send_msg(enum drfz_cmd_t cmd, void* msg, size_t len) {
    return send_msg_with_header(cmd, NULL, 0, msg, len);
}

int recv_msg(void* msg, size_t len) {
    struct drfz_msg_t header = { 0 };

    recv(client_sock, &header, sizeof(header), 0);

    if (header.length != len) {
        drfz_printf("on recv: expected length (%d) != received length (%d)\n",
                header.length, len);
    }

    return recv(client_sock, msg, len, 0);
}

int send_state_regs(dr_mcontext_t* mc)
{
    int sent;

    reg_t regs[8] = {
       mc->xax, mc->xbx, mc->xcx, mc->xdx,
       mc->xsi, mc->xdi, mc->xbp, mc->xsp,
    };

    sent = send_msg(SEND_STATE_REGS_CMD, regs, sizeof(regs));
}

int send_memory(procmaps_struct* procmap) {

    int sent = 0;
    void* start = procmap->addr_start;
    size_t length = procmap->length;

    drfz_printf("Sending %zu bytes from addr %p, module %s\n", length, start, procmap->pathname);

    struct memory_map_msg_t map = {
        .start = start,
        .length = length
    };

    sent += send_msg(SEND_STATE_MEMORY_CMD_HEADER, &map, sizeof(map));

    if (first_run)
        sent += send_msg(SEND_STATE_MEMORY_CMD, start, length);

    return sent;
}
