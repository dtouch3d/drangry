#include "dr_api.h"
#include "drmgr.h"
#include "drwrap.h"
#include "umbra.h"
#include "drutil.h"
#include "drx.h"

#include "dreyfuzz.h"
#include "hook/hook.h"
#include "instrumentation.h"
#include "options.h"
#include "ipc.h"
#include "log.h"

#include <string.h>

void
event_exit(void)
{
    shadow_memory_destroy();

    if (!drmgr_unregister_module_load_event(event_module_load) ||
        !drmgr_unregister_bb_insertion_event(event_bb_insert))
    {
        DR_ASSERT_MSG(false, "error unregistering events");
    }

    close_log_file();

    umbra_exit();
    drwrap_exit();
    drreg_exit();
    drmgr_exit();
    drutil_exit();
    drx_exit();
    cleanup_ipc();
}

DR_EXPORT void
dr_client_main(client_id_t id, int argc, const char *argv[])
{
    int ask_argc;
    int ret;
    const char** ask_argv;

    dr_set_client_name("/)R.Eyfuzz", "@dtouch3d");
    drfz_printf("/)R.Eyfuzz\n");

    bool ok = dr_get_option_array(id, &ask_argc, &ask_argv);

    if (ok)
        parse_options(ask_argc, ask_argv);
    else
        drfz_printf("Could not parse options! error %d\n", ok);

    if (drfz_opts.input_file)
        drfz_printf("Using file : %s\n", drfz_opts.input_file);
    else
        drfz_printf("No input file, using stdin ...\n");

    create_log_file();

    drmgr_init();
    drutil_init();
    drwrap_init();
    umbra_init(id);
    drx_init();

    drmgr_priority_t priority = {
        .struct_size = sizeof(priority),
        .name = "/)R.Eyfuzz",
        .before = NULL,
        .after = NULL,
        .priority = 0
    };

    drreg_options_t drreg_ops = {
        .struct_size = sizeof(drreg_options_t),
        .num_spill_slots = 3,
        .conservative = false
    };

    if (!drmgr_register_module_load_event(event_module_load) ||
        !drmgr_register_bb_instrumentation_event(NULL, event_bb_insert, &priority) ||
        drreg_init(&drreg_ops) != DRREG_SUCCESS) {
        DR_ASSERT(false);
        return;
    }

    dr_register_exit_event(event_exit);

    shadow_memory_init();

    tls_index = drmgr_register_tls_field();
    DR_ASSERT(tls_index != -1);
}
