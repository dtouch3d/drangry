#include "dr_api.h"
#include "drx.h"
#include <string.h>

#include "log.h"
#include "options.h"

char logfile[MAX_FILENAME_LENGTH];
file_t logfd = INVALID_FILE;

void create_log_file(void)
{
    if (drfz_opts.log) {
        logfd = drx_open_unique_appid_file("./", dr_get_process_id(), "dreyfuzz", "log",
                DR_FILE_ALLOW_LARGE, logfile, sizeof(logfile));

        if (logfd == INVALID_FILE)
            drfz_printf("Could not create logfile\n");
        else
            drfz_printf("Writing log to %s\n", logfile);
    }
}

void close_log_file(void)
{
    if (logfd != INVALID_FILE)
        dr_close_file(logfd);
}

void drfz_log(char* fmt, ...)
{
    char line[MAX_LINE_LENGTH];
    va_list argptr;

    if (logfd != INVALID_FILE) {

        va_start(argptr, fmt);
        dr_vsnprintf(line, sizeof(line), fmt, argptr);
        va_end(argptr);

        line[MAX_LINE_LENGTH-1] = '\0';

        dr_write_file(logfd, line, strlen(line));
    }
}
