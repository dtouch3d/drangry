#include <stdint.h>

extern char* input_filepath;
extern int input_fd;
extern void* input_file_ptr;
extern size_t already_read;

void
pre_open(void *wrapcxt, void** user_data);

void
post_open(void *wrapcxt, void** user_data);

void
pre_read(void *wrapcxt, void** user_data);

void
post_read(void *wrapcxt, void** user_data);

void
pre_fopen(void *wrapcxt, void** user_data);

void
post_fopen(void *wrapcxt, void** user_data);

void
pre_fread(void *wrapcxt, void** user_data);

void
post_fread(void *wrapcxt, void** user_data);

void
pre_close(void *wrapcxt, void** user_data);

void
send_app_state(void* wrapcxt);

int
init_shadow_taint(void* buf, size_t size);

int
pre_input_send(void* wrapcxt, void* buf, size_t size);
