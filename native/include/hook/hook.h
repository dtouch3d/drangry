#ifndef _HOOK_H
#define _HOOK_H

#include "dr_api.h"

#define MAX_FUNCTIONS 32
#define MAX_FUNCTION_NAME 64

struct function_info {
    char name[MAX_FUNCTION_NAME];
    void* addr;
    void* pre_func;
    void* post_func;
};

struct module_info {
    char* name;
    struct function_info functions[MAX_FUNCTIONS];
};

void
event_module_load(void *drcontext, const module_data_t *info, bool loaded);

void
pre_main_shadow_stack_init(void* wrapcxt, void** userdata);

void
pre_libc_start_main(void* wrapcxt, void** userdata);

void
pre_skip_call_success(void* wrapcxt, void** userdata);

void unhook(char* func);

#endif
