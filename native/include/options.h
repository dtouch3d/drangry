#ifndef _OPTIONS_H
#define _OPTIONS_H

#include "dr_api.h"

struct dreyfuzz_options_t {
    int log;
    int verbose;
    const char* input_file;
};

extern struct dreyfuzz_options_t drfz_opts;

void parse_options(int argc, const char *argv[]);

#endif
