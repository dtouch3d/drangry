#include "dr_api.h"
#include "pmparser.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <unistd.h>

static int client_sock = 0;
int tag;
int first_run;

enum drfz_cmd_t {
    INITIALIZE_CMD,
    SEND_STATE_MEMORY_CMD_HEADER,
    SEND_STATE_REGS_CMD,
    SEND_CBR_CMD,
    SEND_TAINT_HIT_COUNT_CMD,
    CONTINUE_CMD,
    SEND_STATE_MEMORY_CMD,
    INPUT_BUFFER_CMD,
    SEND_READ_RETADDR_CMD,
    INPUT_FILE_CMD,
    FILE_OFFSET_CMD,
    SEND_LIB_INFO_CMD,
    SEND_MAGIC_INPUT_CMPS
};

struct drfz_msg_t {
    int tag;
    enum drfz_cmd_t cmd;
    size_t length;
};

struct init_msg_t {
    void* main_module_start;
};

struct memory_map_msg_t {
   void* start;
   size_t length;
};

struct state_msg_t {
    int regs[16];
};

struct cbr_msg_t {
    app_pc instr_addr;
    app_pc target_addr;
    app_pc fall_addr;
    int taken;
};

struct lib_msg_t {
    app_pc addr_start;
    char name[128];
};

int initialize_ipc(void);
int cleanup_ipc(void);

int send_msg_with_header(enum drfz_cmd_t cmd, void* header, size_t header_len, void* msg, size_t msg_len);
int send_msg(enum drfz_cmd_t cmd, void* msg, size_t len);
int recv_msg(void* msg, size_t len);
int send_memory(procmaps_struct* procmap);
int send_state_regs(dr_mcontext_t *mcontext);
