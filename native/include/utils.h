#ifndef UTILS_H
#define UTILS_H

#include "dr_api.h"

#define MAX_FILENAME_LENGTH 1024
#define MAX_LINE_LENGTH 1024
#define SZ_1MB 1024*1024

#define ARRAY_SIZE(x) sizeof(x)/sizeof(x[0])

#define dr_printf_ptr(var) dr_printf("%s : %p\n", #var, var)
#define drfz_printf(fmt, ...) dr_printf("[dreyfuzz] " fmt, ##__VA_ARGS__)
#define drfz_log_ptr(var) drfz_log("%s : %p\n", #var, var)

bool instr_is_mov_or_movzx(instr_t* instr);
bool instr_is_cmp(instr_t* instr);
bool instr_is_arithmetic(instr_t* instr);
reg_id_t get_canonicalized_reg(reg_id_t reg);
const char* get_module_name_from_instr(instr_t* instr);
const char* instr_to_memop(instr_t *instr);
void log_disassembly_line(void* drcontext, instr_t* instr);
unsigned int opnd_size_bytes(opnd_t* opnd);
unsigned int opnds_get_min_size(opnd_t* opnd1, opnd_t* opnd2);
bool instr_in_blacklist(instr_t* instr);
void print_mcontext(dr_mcontext_t *mc);
void hexdump(const void* data, size_t size);

#endif
