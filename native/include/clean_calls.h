#ifndef _INSTRUMENTATION_H
#define _INSTRUMENTATION_H


void pre_insert_get_mem_addr(void *drcontext, instrlist_t *ilist, instr_t *where, opnd_t *memref,
        reg_id_t *reg_addr);

void post_insert_get_mem_addr(void *drcontext, instrlist_t *ilist, instr_t *where,
        reg_id_t reg_addr);

void clean_call_memref_to_reg(void* src_mem_addr, unsigned long dst_reg, unsigned int size);
void clean_call_reg_to_memref(void* dst_mem_addr, unsigned long reg_src,
        unsigned int size);
void clean_call_imm_to_reg(int reg_dst);
void clean_call_imm_to_memref(void* dst_mem_addr, size_t opnd_size);
void clean_call_reg_to_reg(int reg_src, int reg_dst);
void clean_call_memref_to_memref(void* dst_mem_addr, void* src_mem_addr, unsigned int size);
void clean_call_multireg_to_reg(int reg_src, int reg1, int reg2, int reg3,
        int reg4);

void clean_call_cbr(app_pc instr_addr, app_pc target_addr, app_pc fall_addr, int taken, void* dummy);

void clean_call_inc_hitcount_mem(void* mem, unsigned int size, int immed);
void clean_call_inc_hitcount_reg(reg_t reg, unsigned int size, int immed);

#endif
