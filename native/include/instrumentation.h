#ifndef TAINT_H
#define TAINT_H

#include "dr_api.h"
#include "umbra.h"
#include "drreg.h"
#include <stdint.h>

#define MAX_DISASSM_SIZE 128

/* We use 2 shadow bytes per application byte, holding the index
 * of the byte in the original input buffer.
 */
typedef uint16_t shadow_value_t;

#define DRFZ_UMBRA_MAP_SCALE UMBRA_MAP_SCALE_UP_2X

/* DynamoRIO fails for an initial value equal to UCHAR_MAX
 */

#define UNTAINTED_SHADOW_VALUE 0xfe
#define UNTAINTED_SHADOW_VALUE_SHORT (UNTAINTED_SHADOW_VALUE << 8 | UNTAINTED_SHADOW_VALUE)
#define SHADOW_VALUE_SIZE sizeof(shadow_value_t)

#define MAGIC_IGNORE 0x55555555

extern const char* taint_module_blacklist[];

umbra_map_t* umbra_map;

/* Number of scratch registers needed by umbra for translation in the
 * in the instrumentation level. For x86 = 1, for x64 = 0, we can safely
 * assume a maximum of 1.
 */
int umbra_num_scratch_regs;

/* We define a large array so we can index it directly with
 * DR_REG_XXX for taint propagation
 */
#define MAX_REG_NUM 8*4096

/* Holds taint value for registers, access with DR_REG_XXX as index */
extern shadow_value_t shadow_reg_array[MAX_REG_NUM];

extern int taint_hit_count1[UNTAINTED_SHADOW_VALUE_SHORT];
extern int taint_hit_count2[UNTAINTED_SHADOW_VALUE_SHORT];

/* In position N we hold the immediate values that were
 * compared to the input byte in position N
 */

extern int magic_input_cmps[UNTAINTED_SHADOW_VALUE_SHORT+1];

extern int* curr_hitcount;
extern int* last_hitcount;

extern size_t bytes_read;

void shadow_memory_init(void);
void shadow_memory_destroy(void);

void insert_memref_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_src, opnd_t* opnd_dst);

void insert_reg_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_src, opnd_t* memref_dst);

void insert_memref_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_src, opnd_t* memref_dst);

void insert_reg_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_src, opnd_t* opnd_dst);

void insert_immed_to_reg(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* opnd_dst);

void insert_immed_to_memref(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* memref_dst);

dr_emit_flags_t event_bb_insert(void *drcontext, void *tag, instrlist_t
        *bb, instr_t *instr, bool for_trace, bool translating, void *user_data);

void handle_mov_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst);

void handle_arithmetic_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst);

void handle_lea_instr(void* drcontext, instrlist_t* bb, instr_t* instr,
        opnd_t* src, opnd_t* dst);

void insert_inc_mem_hitcount(void* drcontext, instrlist_t* bb, instr_t* instr, opnd_t* opnd, opnd_t* cmp);

void insert_inc_reg_hitcount(void* drcontext, instrlist_t* bb, instr_t* instr, opnd_t* opnd, opnd_t* cmp);

void print_all_shadow_mem(void* pc);
void handle_cbr(app_pc instr_addr, app_pc targ_addr, app_pc fall_addr, int taken, void* dummy);
bool iter_print_shadow_mem(umbra_map_t *map, umbra_shadow_memory_info_t *info, void *user_data);

#endif
