#ifndef _LOG_H
#define _LOG_H

#include "dr_api.h"
#include "utils.h"

extern char logfile[MAX_FILENAME_LENGTH];
extern file_t logfd;

void create_log_file(void);
void close_log_file(void);
void drfz_log(char* fmt, ...);

#endif
