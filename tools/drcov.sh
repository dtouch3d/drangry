#!/usr/bin/env bash

if [ $# -ne 2 ]
then
    echo "usage: ./drcov.sh <binary> <testcases>"
    exit
fi

echo $1
echo $2

DR_PATH="../native/prebuilts/dynamorio/"
DR_RUN="$DR_PATH/bin32/drrun"

BINARY="$1"
BINARY_DIR="$(dirname $BINARY)"
export LD_LIBRARY_PATH=$BINARY_DIR

TESTCASES="$2"
echo "testcases: $TESTCASES"

shift
shift

SUFFIX="_drcov"
LOG_DIR="$TESTCASES$SUFFIX"
echo $LOG_DIR

mkdir $LOG_DIR

for t in $TESTCASES/*;
do
    $DR_RUN -t drcov -dump_text -logdir $LOG_DIR -- "$BINARY" $t
done
